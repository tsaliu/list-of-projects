# List of Projects
This repository consists only a list of projects including the private projects. They all link to their repository. Request for access.

## [SUMO Data](https://gitlab.com/tsaliu/sumo-data) (Public)
Dataset used in [Final Anomaly Detection](https://gitlab.com/tsaliu/final_anomaly_detection) generated from [Final SUMO Anomaly Simulator](https://gitlab.com/tsaliu/final_sumo_anomaly_simulator) 

## [Final Anomaly Detection](https://gitlab.com/tsaliu/final_anomaly_detection) (Private)
Final Version of Anomaly Detection

## [Final SUMO Anomaly Simulator](https://gitlab.com/tsaliu/final_sumo_anomaly_simulator) (Private)
Parameterized all settings with XML and improved algorithms from "SUMO Life".

## [SUMO Life](https://gitlab.com/tsaliu/sumo_life) (Private)
Traffic simulation with anomalies and anomaly detection with pattern of life extraction

## [Anomal SUMO](https://gitlab.com/tsaliu/Anomal_SUMO) (Private)
Simulation of anomal traffic.

## [Final Anomaly Detection](https://gitlab.com/tsaliu/final_anomaly_detection)
Improved version of Anomlay. With instantanious anomalies, and better utilize Patter of Life Extraction

## [Anomaly](https://gitlab.com/tsaliu/Anomaly) (Private)
Anomaly detection algorithm and traffic behaviour classification.

## [pcap-proc](https://gitlab.com/tsaliu/pcapng-proc) (Private)
Live capture or process pcap/pcapng files offline. Saving packets into database to process later. Plot data with python or gnuplot.

## [gpscpp](https://gitlab.com/tsaliu/gpscpp) (Private)
GPS simulator in cpp, translated from gpsmatlab and completed the IMU simulator. gpsmatlab is a matlab gps simulator by etflab, modded and fixed bugs.

## [Kismet-Plugin-Distance](https://gitlab.com/tsaliu/Kismet-Plugin-Distance) 
Plugin for Kismet to calculate Distance of devices and display results with easy interface.

## [Multi-Target-Tracking](https://gitlab.com/tsaliu/Multi-Target-Tracking)
Multi Target Tracking simulating radar. Used EKF, IPDA.

## [Pedestrian-Tracker](https://gitlab.com/tsaliu/Pedestrian-Tracker) 
Simple tracker using MOG2, KF, GNN, Logic Based Track Management.

## [Hurrican-Tracker](https://gitlab.com/tsaliu/Hurrican-Tracker) 
Hurrican Tracker using real work data (Harvey Hurrican Aug 2017 - Sept 2017) Using KF, Fading Memory Average

## [pcap-proc-win](https://gitlab.com/tsaliu/pcap-proc-win) (Private)
processing pcap on windows

## [auto-tcpdump](https://gitlab.com/tsaliu/auto-tcpdump)
simple script to put interface into monitor mode and start tcpdump

## [2.4G-Channel-Hopping](https://gitlab.com/tsaliu/2.4G-Channel-Hopping)
Simple script for channel hopping in 2.4G

## [Capstone-Project](https://gitlab.com/tsaliu/Capstone-Project)
Capstone Project with SUNLAB and iBIONICS at University of Ottawa




